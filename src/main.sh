#!/usr/bin/bash

OWNER="gheshu"
REPO="image_decompiler"
API_TOKEN=""

GH_API="https://api.github.com"
GH_REPO="$GH_API/repos/$OWNER/$REPO"
GH_TAGS="$GH_REPO/releases/latest"
AUTH="Authorization: token $API_TOKEN"

# test the api token
curl -o /dev/null -sH "$AUTH" $GH_REPO || { echo "Error: Invalid repo, token or network issue!"; exit 1; }

pushd /c/Users/Lauren/workspace/$REPO

#while true;
#do
    git fetch
    LAST_COMMIT=`git log -1 --format="%at" | xargs -I{} date +%s -d @{}`
    echo $LAST_COMMIT
    LAST_RELEASE=`curl -s https://api.github.com/repos/$OWNER/$REPO/releases/latest | jq -r ".created_at"`
    LAST_RELEASE=`date +%s --date="$LAST_RELEASE"`
    echo $LAST_RELEASE

    if (( LAST_COMMIT > LAST_RELEASE )); then
        #get latest
        git pull

        # build it
        ./compile.sh

        # cleanup any unwanted files
        cd bin/Release
        rm -rf screenshots/*
        rm -rf *.png
        rm -rf *.jpg
        rm Thumbs.db

        cp ../../assets/*.png .

        cd ..

        rm *.zip

        # zip it up
        FILENAME="ImageDecompiler_Win64_$LAST_COMMIT.zip"
        7z a -tzip -mx9 "$FILENAME" ./Release

        # describe the release
        TAG_NAME="$LAST_COMMIT"
        CUR_TIME=`date`
        RELEASE_NAME="Release on $CUR_TIME"
        RELEASE_DESC="Build created on $CUR_TIME for Win64 based on latest commit on master branch."

        RELEASE_JSON="{
            \"tag_name\": \"$TAG_NAME\",
            \"target_commitish\": \"master\",
            \"name\": \"$RELEASE_NAME\",
            \"body\": \"$RELEASE_DESC\",
            \"draft\": false,
            \"prerelease\": false
        }"

        echo $RELEASE_JSON

        # create release
        curl -v -i -X POST -H "Content-Type:application/json" -H "Authorization: token $API_TOKEN" "https://api.github.com/repos/$OWNER/$REPO/releases" -d "$RELEASE_JSON"

        # get release ID
        response=$(curl -sH "$AUTH" $GH_TAGS)
        ASSET_ID=`echo $response | jq -r ".id"`
        
        # upload release asset
        GH_ASSET="https://uploads.github.com/repos/$OWNER/$REPO/releases/$ASSET_ID/assets?name=$FILENAME" 
        curl -H "Authorization: token $API_TOKEN" -H "Content-Type: application/zip" --data-binary @"$FILENAME" $GH_ASSET

        cd ..
    fi

    sleep 3600
#done

popd